//Создать массив заполнить его случайными элементами, распечать, перевернуть, и снова распечатать
package BezOcenki;

import java.util.Scanner;

public class MassReverse {
    public static void main(String[] args) {
        int[] mass;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину массива n");
        int n = scanner.nextInt();
        mass = new int[n];
        for (int i = 0; i < mass.length; i++) {
            mass[i] = (int) (Math.random() * 10);
            System.out.print(" " + mass[i]);
        }
        System.out.println(" ");
        for (int i = 0; i < mass.length / 2; i++) {
            int b = mass[i];
            mass[i] = mass[mass.length - 1 - i];
            mass[mass.length - i - 1] = b;
        }
        for (int i = 0; i < mass.length; i++) {
            System.out.print(" " + mass[i]);
        }
    }
}

//Напишите программу расчета даты следующего дня введя 3 числа - день месяц и год
//Примеры работы программы:
//15
//3
//2000
//16 3 2000
//31
//12
//1999
//1 1 2000
package BezOcenki;

import java.util.Scanner;

public class NextDay {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Vvedite daty");
        int a = scanner.nextInt();
        System.out.print("Vvedite mesyac");
        int b = scanner.nextInt();
        System.out.print("Vvedite god");
        int c = scanner.nextInt();
        //Дата в апреле,июне,сентябре,ноябре
        if (b == 4 || b == 6 || b == 9 || b == 11) {
            if (a < 30) {
                a = a + 1;
                System.out.print(a + " ");
                System.out.print(b + " ");
            } else if (a == 30) {
                a = 1;
                b = b + 1;
                System.out.print(a + " ");
                System.out.print(b + " ");
            }
            System.out.print(c);
            return;
        }
        //Дата в феврале
        if (b == 2 && c % 4 != 0) {
            if (a < 28) {
                a = a + 1;
                System.out.print(a + " ");
                System.out.print(b + " ");

            } else if (a == 28) {
                a = 1;
                b = b + 1;
                System.out.print(a + " ");
                System.out.print(b + " ");
            }
            System.out.print(c);
            return;
        } else if (b == 2) {
            if (a < 29) {
                a = a + 1;
                System.out.print(a + " ");
                System.out.print(b + " ");
            } else if (a == 29) {
                a = 1;
                b = b + 1;
                System.out.print(a + " ");
                System.out.print(b + " ");
            }
            System.out.print(c);
            return;
        }
        //Дата в остальне месяцы
        if (b == 1 || b == 3 || b == 5 || b == 7 || b == 8 || b == 10 || b == 12) {
            if (a < 31) {
                a = a + 1;
                System.out.print(a + " ");
                System.out.print(b + " ");

            } else if (a == 31) {
                a = 1;
                b = b + 1;
                if (b >= 12) {
                    b = 1;
                    c = c + 1;
                }
                System.out.print(a + " ");
                System.out.print(b + " ");
            }
            System.out.print(c);

        }
    }
}

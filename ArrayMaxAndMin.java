//Найти максимальный и минимальные элементы массива и вывести их индексы на экран
package BezOcenki;
public class ArrayMaxAndMin {
    public static void main(String[] args) {
        double[] mass = {-1, 2, 58, -11.2, 2, -66.5};
        double max = mass[0];
        double min = mass[0];
        for (int i = 0; i < mass.length; i++) {
            if (mass[i] > max) {
                max = mass[i];
            }
            if (mass[i] < min) {
                min = mass[i];
            }
        }

        System.out.println("Максимальный элемент массива:" + max + ";минимальный элемент массива:" + min + ".");
    }
}
//Имеется прямоугольное отверстие размера a на b.
//Определить можно ли полностью закрыть отверстие круглой картонкой радиусом r
//Примеры работы программы:
//6
//8
//5
//Картонка с радиусом 5 закрывает полностью отверстие размера 6 * 8
//3
//4
//2
//Картонка с радиусом 2 не закрывает полностью отверстие размера 3 * 4
package BezOcenki;

import java.util.Scanner;

public class Otv {
    public static void main(String[] args) {
        System.out.println("Введите длину сотроны а");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        System.out.println("Введите длину сотроны b");
        int b = scanner.nextInt();
        System.out.println("Введите радиус r");
        int r = scanner.nextInt();
        if ((double) 2*r >= Math.sqrt(a * a + b * b) ) {
            System.out.println("Картонка с радиусом " + r + "  закрывает полностью отверстие размера " + a + "*" + b);
        } else {
            System.out.println("Картонка с радиусом " + r + " не закрывает полностью отверстие размера " + a + "*" + b);
        }
    }
}
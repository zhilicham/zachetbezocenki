//Для введенного целого числа определить является ли это число простым
package BezOcenki;
import java.util.Scanner;
public class Simple {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число");
        int a = scanner.nextInt();
        int z=0;
        if (a == 1) {
            System.out.println("Число "+a+" не простое");
            return;
        }
        if (a == 2) {
            System.out.println("Число "+a+" простое");
            return;
        }
        for (int i = 2; i*i  <= a; i++) {
            if (a % i ==0){
                z=1;
                break;
            }
        }
        if (z == 0) {
            System.out.println("Число "+a+" простое");
        } else {
            System.out.println("Число "+a+" составное");
        }
    }
}
